# Slab.css
Slab.css it a lightweight CSS toolkit based on Inuit.css/ITCSS.

## Getting started
Install Slab.css via yarn or bower and include in you main scss file as follows:

```scss
@import "path/to/slab/tools";
@import "vars"; // Your variable overrides go here
@import "path/to/slab/index";

@import "path/to/slab/plugins";
// Your application styles go here
```

As your variable overrides can potentially use values specified in the various data maps, the functions to access these data maps need to be added first.

## Settings
Slab.css handles 3 aspects of your application; breakpoints, colours and spacing. Each of these provide variables that can be overridden to suit application's design.

## Responsive
Slab.css manages its breakpoints and responsive helper classes via a sass data map `$breakpoints`. The data map allows you to set a media query and give it a name allowing it to be referenced easier throughout your application.

```scss
$breakpoints: (
    palm: '(max-width: 767px)',
    lap: '(min-width: 768px) and (max-width: 1023px)',
    portable: '(max-width: 1023px)',
    lap-and-up: '(min-width: 768px)',
    desk: '(min-width: 1024px)',
    desk-wide: '(min-width: 1440px)'
) !default;
```

The defaults shown above are accessed via the media-query sass mixin:

```scss
.box {
    padding-top: 1rem;

    @include media-query(lap-and-up) {
        padding-top: 2rem;
    }

    @include media-query(desk-wide) {
        font-size: 1.5rem;
    }
}
```

## Colours
Managing colours in Slab.css is done via a sass data map named `$palette`. The map allows for nicer management than standard sass variables and also creates helper classes to apply text and background colours to any element.  Each colour added must have a tone named `base` at a minimum.

Create map:
```scss
$palette: (
    'grey': (
        'base': #999,
        'light': lighten(#999, 25%),
        'dark': #333
    ),
    'red': (
        'base': #F00
    ),
);
```

 Access to the map is via the `getColour` mixin.

```scss
.box {
    background-color: getColour('red'); // 'base' is default
    color: getColour('grey', 'dark');
}
```

A set of utility classes are also generated.

```html
<div class="bg--grey-light text--red"></div>
```

## Spacing
Slab.css allows for the creation of utility classes and sass variables to help with spacing. A data map is used to create references to spacing values of your choosing. By default the data map contains:

```scss
$spacing: (
    'small': 0.75rem,
    'medium': 1.5rem,
    'large': 3rem
) !default;
```

From these values, responsive utility classes are created for both margin and padding in the format `{namespace}-{margin/padding}{direction}-{data map key}’.

If we assume 1rem is 16px, then:

```scss
.m-small  // margin: 12px;
.ml-none // margin-left: 0;
.px-medium // padding-left: 24px; padding-right: 24px;
.py-large // padding-top: 48px; padding-bottom: 48px;

// Responsive utility classes
.portable-mb-none // margin-bottom: 0; on portable breakpoint
.desk-wide-p-medium // padding: 24px; on desk-wide breakpoint
```

*Note:* Each value in the spacing data map can produce a large amount of css. If a value is not used in your application it is recommended to overwrite the data map in your `_vars.scss` with the appropriate values to reduce file size. Or alternatively if your application does not make use of the responsive utility classes you can switch them off via the `$slab-responsive-spacing` boolean variable.

## Visibility
Responsive visibility helper classes are available to hide/show content based on breakpoints.

```scss
.hide-palm // hide on palm breakpoint
.show-lap-and-up // show on lap-and-up breakpoint
```

